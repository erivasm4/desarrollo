package com.desarrolloweb.spring.app.controllers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;


import com.desarrolloweb.spring.app.repositories.ProveedorRepository;
import com.desarrolloweb.spring.app.repositories.UserServices;
import com.desarrolloweb.spring.app.util.PageRender;
import com.desarrolloweb.spring.app.view.excel.ProveedorExcelExporter;
import com.desarrolloweb.spring.app.view.excel.ProveedorPDFExporter;
import com.lowagie.text.DocumentException;
import com.desarrolloweb.spring.app.entities.Audit;

import com.desarrolloweb.spring.app.entities.Proveedor;

@Controller
public class ProveedorController {

	@Autowired
	private ProveedorRepository proveedorRepository;
	
	@Autowired
    private UserServices service;

	@RequestMapping(value = "/detalle-proveedor/{id}", method = RequestMethod.GET)
	public String detalleProveedor(@PathVariable(value = "id") Long id, Model model) {

		Proveedor proveedor = proveedorRepository.findById(id).get();
		if (proveedor == null) {
			return "redirect:/listar-proveedores";
		}

		model.addAttribute("titulo", "Detalle Proveedor: " + proveedor.getNombre());
		model.addAttribute("proveedor", proveedor);
		return "detalle-proveedor-form";
	}
	
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/listar-proveedores", method = RequestMethod.GET)
	public String listarProveedores(@RequestParam(name = "page", defaultValue = "0") int page, Model model) {

		Pageable pageRequest = PageRequest.of(page, 4);

		Page<Proveedor> proveedores = proveedorRepository.findAll(pageRequest);

		PageRender<Proveedor> pageRender = new PageRender<Proveedor>("/listar-proveedores", proveedores);
		model.addAttribute("titulo", "Listado de proveedores");
		model.addAttribute("proveedores", proveedores);
		model.addAttribute("page", pageRender);
		return "proveedores";
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	@RequestMapping(value = "/nuevo-proveedor", method = RequestMethod.GET)
	public String nuevoProveedor(Model model) {
		Proveedor proveedor = new Proveedor();
		model.addAttribute("titulo", "Nuevo Proveedor");
		model.addAttribute("proveedor", proveedor);
		return "form-proveedor";
	}

	@RequestMapping(value = "/editar-proveedor/{id}", method = RequestMethod.GET)
	public String editarProveedor(@PathVariable(value = "id") Long id, Model model) {
		Proveedor proveedor = null;
		if (id > 0) {
			proveedor = proveedorRepository.findById(id).get();
		} else {
			return "redirect:/listar-proveedores";
		}
		model.addAttribute("titulo", "Editar Proveedor");
		model.addAttribute("proveedor", proveedor);
		return "form-proveedor";
	}

	@RequestMapping(value = "/eliminar-proveedor/{id}", method = RequestMethod.GET)
	public String eliminarProveedor(@PathVariable(value = "id") Long id, Model model) {
		Proveedor proveedor = null;
		if (id > 0) {
			proveedor = proveedorRepository.findById(id).get();
			proveedorRepository.delete(proveedor);
		} else {
			return "redirect:/listar-proveedores";
		}

		return "redirect:/listar-proveedores";
	}

	@RequestMapping(value = "/nuevo-proveedor", method = RequestMethod.POST)
	public String guardarProveedor(Proveedor proveedor) {
		Audit audit = null;
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();


		if (proveedor.getId() != null && proveedor.getId() > 0) {
			Proveedor proveedor2 = proveedorRepository.findById(proveedor.getId()).get();
			audit = new Audit(auth.getName());
			proveedor.setAudit(audit);
			proveedor.setId(proveedor2.getId());
			proveedor.getAudit().setTsCreated(proveedor2.getAudit().getTsCreated());
			proveedor.getAudit().setUsuCreated(proveedor2.getAudit().getUsuCreated());
		} else {
			audit = new Audit(auth.getName());
			proveedor.setAudit(audit);
		}

		proveedorRepository.save(proveedor);
		return "redirect:/listar-proveedores";
	}
	
	@GetMapping("/proveedores-export-excel/")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date(0));
         
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=proveedores_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
         
        List<Proveedor> listProveedores = service.listAll();
         
        ProveedorExcelExporter excelExporter = new ProveedorExcelExporter(listProveedores);
         
        excelExporter.export(response);    
    }  
	@GetMapping("/proveedores-export-pdf/")
    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
        response.setContentType("application/pdf");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date(0));
         
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=proveedores_" + currentDateTime + ".pdf";
        response.setHeader(headerKey, headerValue);
         
        List<Proveedor> listProveedores = service.listAll();
         
        ProveedorPDFExporter exporter = new ProveedorPDFExporter(listProveedores);
        exporter.export(response);
         
    }

}

package com.desarrolloweb.spring.app.repositories;
 
import java.util.List;
 
import javax.transaction.Transactional;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.desarrolloweb.spring.app.entities.Proveedor;
 
@Service
@Transactional
public class UserServices {
     
    @Autowired
    private ProveedorRepository repo;
     
    public List<Proveedor> listAll() {
        return (List<Proveedor>) repo.findAll();
    }
     
}